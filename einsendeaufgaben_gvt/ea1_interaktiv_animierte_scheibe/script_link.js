/**
 * Created by Jonas on 01.04.2017.
 */
"use strict";
let imageArrayLink = [];
let linkCounter = 0;
let NUMBER_OF_IMAGES = 11;

//logic similar to script_scheiben.js - only event is on "onkeydown"
(function fillLinkImageArray() {
    let imageBasicUrl = "images/link/link_";
    for (let i = 0; i <= NUMBER_OF_IMAGES; i++) {
        let imageUrl = imageBasicUrl + i;
        let imageObj = new Image();
        imageObj.src = imageUrl + ".png";
        imageArrayLink.push(imageObj);
    }
})();

window.onkeydown = function (e) {
    if (e.key === " ") {
        linkCounter += 1;
        if (linkCounter < imageArrayLink.length) {
            loadImageLink(imageArrayLink[linkCounter]);
        }
        else {
            linkCounter = 0;
            loadImageLink(imageArrayLink[linkCounter]);
        }
    }
}

// fillLinkImageArray();
function loadImageLink(imageObject) {
    let imgTag = document.querySelector('#y');
    imgTag.setAttribute('src', imageObject.src);
}
