"use strict";

let imageArrayScheiben = [];
let scheibenCounter = 0;
let NEXT_DEGREE_STEP = 15;
let FINAL_DEGREE_STEP = 345;

//replace the image Tag-src with the image Object from onkeyup function
function loadImage(imageObject) {
    let imgTag = document.querySelector('#x');
    imgTag.setAttribute('src', imageObject.src);
}

// warmup array - fill it with every image in folder - the approach through the filename is necessary
// because of the impossibility from JS to load every file in a folder WITHOUT any external LIB (AJAX, file ...)
(function fillScheibenImageArray() {
    let imageBasicUrl = "images/scheiben/scheibe_";
    for (let i = 0; i <= FINAL_DEGREE_STEP; i += NEXT_DEGREE_STEP) {
        let imageUrl = imageBasicUrl + i;
        let imageObj = new Image();
        imageObj.src = imageUrl + ".png";
        imageArrayScheiben.push(imageObj);
    }
})();

// Listener for user input -
// on the R movement - the Counter must not get under the max number in Array (length-1) - else it should set it to 0
// on the L movement - reversed logic from r
window.onkeyup = function (e) {
    if (e.key === "r" || e.key === "R") {
        scheibenCounter += 1;
        if (scheibenCounter <= imageArrayScheiben.length - 1) {
            loadImage(imageArrayScheiben[scheibenCounter]);
        }
        // sets scheibenCounter to start when maximum number is reached - so that array-Iteration starts from beginning
        else {
            loadImage(imageArrayScheiben[0]);
            scheibenCounter = 0;
        }
    }
    if (e.key === "l" || e.key === "L") {
        scheibenCounter -= 1;
        if (scheibenCounter >= 0) {
            loadImage(imageArrayScheiben[scheibenCounter])
        }
        //sets scheibenCounter to max of array-Iteration
        else {
            scheibenCounter = imageArrayScheiben.length - 1;
            loadImage(imageArrayScheiben[scheibenCounter])
        }
    }
};

