Projektaufgabe: Toon-Shader

Voraussetzungen: Lerneinheiten 1-14 bis einschlie�lich BEL.

Bearbeitungszeit: 60 Minuten

Aufgabenstellung: Beleuchten Sie die Beispielszene aus der Lerneinheit BEL im Stil eines traditionellen Zeichentrickfilms mit einem Toon-Shader (auch als Cel-Shader bezeichnet). Was das ist und wie das geht m�ssen recherchieren, siehe dazu:

http://de.wikipedia.org/wiki/Cel_Shading

http://en.wikipedia.org/wiki/Cel_shading

http://en.wikibooks.org/wiki/GLSL_Programming/Unity/Toon_Shading

Material: Keines.

Bewertungskriterien und Punkte:

Die Bewertung erfolgt an Hand der folgenden Kriterien:

Funktionsf�higkeit der Implementierung: 10 Punkte

Gesamtpunktzahl: 10