var octahedron = ( function () {

    function createVertexData() {

        //name added for if-else in drawing method
        this.name = "octahedron";
        var name = this.name;

        var vertices;
        var colors = [];
        this.colors = colors;
        this.points = [];
        var points = this.points;


        // Normals.
        // this.normals = new Float32Array(3 * (n + 1) * (m + 1));
        // var normals = this.normals;
        // Index data.
        this.indicesLines = [];
        var indicesLines = this.indicesLines;
        this.indicesTris = [];
        var indicesTris = this.indicesTris;

        //get the current breakdown value from frontend - if else for first call when local storage is empty
        if (localStorage.getItem("iterations") == null) {
            this.numTimesToSubdivide = 1;
        }
        else {
            this.numTimesToSubdivide = localStorage.getItem("iterations");
        }
        //ugly but fast: when number is changed, innerHTML besides range will be changed to current value and
        // slider set to current value
        document.querySelector("#iterationNumber").innerHTML = this.numTimesToSubdivide;
        document.querySelector("#divisionNumberPicker").value = this.numTimesToSubdivide;
        var numTimesToSubdivide = this.numTimesToSubdivide;

        vertices = [
            [-0.7071, 0.0000, 0.7071],      // 0 - 0 1 2
            [0.0000, 1.0000, 0.0000],      // 1 -3 4 5
            [-0.7071, 0.0000, -0.7071],        // 2 - 6 7 8
            [0.7071, 0.0000, -0.7071],      // 3 - 9 10 11
            [0.7071, 0.0000, 0.7071],      // 4 - 12 13 14
            [0.0000, -1.0000, 0.0000]   // 5 - 15 16 17
        ];


        octahedron(vertices[0], vertices[1], vertices[2], vertices[3],
            vertices[4], vertices[5], numTimesToSubdivide);

        // Set index.
        // Line on beam.
        // if (j > 0 && i > 0) {
        //     indicesLines[iLines++] = iVertex - 1;
        //     indicesLines[iLines++] = iVertex;
        // }
        // // Line on ring.
        // if (j > 0 && i > 0) {
        //     indicesLines[iLines++] = iVertex - (m + 1);
        //     indicesLines[iLines++] = iVertex;
        // }

        // Set index.
        // Two Triangles.
        //
        //
        // this.indicesTris = [
        //     0, 2, 4,
        //     0, 2, 5,
        //     0, 3, 4,
        //     0, 3, 5,
        //     1, 2, 4,
        //     1, 2, 5,
        //     1, 3, 4,
        //     1, 3, 5];
        //
        // this.indicesLines = [
        //     2, 0,
        //     2, 1,
        //     2, 4,
        //     2, 5,
        //     3, 0,
        //     3, 1,
        //     3, 4,
        //     3, 5,
        //
        //     5, 0,
        //     5, 1,
        //     4, 0,
        //     4, 1,
        // ];


        function octahedron(a, b, c, d, e, f, count) {
            //upper part
            divideTriangle(b, a, c, count, 1,0,0,1);
            divideTriangle(b, c, d, count, 1,0,0,1);
            divideTriangle(b, d, e, count, 0,0,1,1);
            divideTriangle(b, e, a, count, 0,0,1,1);
            //lower part
            divideTriangle(f, a, c, count, 0,0,0,1);
            divideTriangle(f, c, d, count, 0,0,0,1);
            divideTriangle(f, d, e, count, 0,0,0,1);
            divideTriangle(f, e, a, count, 0,0,0,1);

        }

        function divideTriangle(a, b, c, count, colorR, colorG, colorB, colorA) {

            // check for end of recursion


            if (count == 0) {
                triangle(a, b, c, colorR, colorG, colorB, colorA);
            }
            else {

                //bisect the sides

                var ab = normalize(mix(a, b, 0.5));
                var ac = normalize(mix(a, c, 0.5));
                var bc = normalize(mix(b, c, 0.5));

                --count;

                // three new triangles

                divideTriangle(a, ab, ac, count, colorR, colorG, colorB, colorA);
                divideTriangle(c, ac, bc, count, colorR, colorG, colorB, colorA);
                divideTriangle(b, bc, ab, count, colorR, colorG, colorB, colorA);
                divideTriangle(ac, ab, bc, count, colorR, colorG, colorB, colorA);

            }
        }

        function triangle(a, b, c, colorR, colorG, colorB, colorA) {
            // add colors and vertices for one triangle

            colors.push( colorR, colorG, colorB, colorA);
            points.push(a);
            colors.push( colorR, colorG, colorB, colorA);
            points.push(a);
            colors.push( colorR, colorG, colorB, colorA);
            points.push(b);
            colors.push( colorR, colorG, colorB, colorA);
            points.push(c);
            colors.push( colorR, colorG, colorB, colorA);
            points.push(a);
            colors.push( colorR, colorG, colorB, colorA);
            points.push(a);
        }

        this.flattenPoints = flatten(points);
        var flattenPoints = this.flattenPoints;


        // for (let i of flattenPoints) {
        //     document.write(i);
        //     document.write(",");
        // }

    }


    return {
        createVertexData: createVertexData,
    }

}());