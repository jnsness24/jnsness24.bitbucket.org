var octahedron = ( function () {

    function createVertexData() {

        // Positions.
        this.vertices = [];
        var vertices = this.vertices;
        // Normals.
        // this.normals = new Float32Array(3 * (n + 1) * (m + 1));
        // var normals = this.normals;
        // Index data.
        this.indicesLines = [];
        var indicesLines = this.indicesLines;
        this.indicesTris =[];
        var indicesTris = this.indicesTris;

        this.vertices = [
            1, 0, 0,      // 0 - 0 1 2
            -1, 0, 0,      // 1 -3 4 5
            0, 1, 0,        // 2 - 6 7 8
            0, -1, 0,      // 3 - 9 10 11
            0, 0, 1,      // 4 - 12 13 14
            0, 0, -1,   // 5 - 15 16 17
        ];

        // Set index.
        // Line on beam.
        // if (j > 0 && i > 0) {
        //     indicesLines[iLines++] = iVertex - 1;
        //     indicesLines[iLines++] = iVertex;
        // }
        // // Line on ring.
        // if (j > 0 && i > 0) {
        //     indicesLines[iLines++] = iVertex - (m + 1);
        //     indicesLines[iLines++] = iVertex;
        // }

        // Set index.
        // Two Triangles.



        this.indicesTris = [
            0, 2, 4,
            0, 2, 5,
            0, 3, 4,
            0, 3, 5,
            1, 2, 4,
            1, 2, 5,
            1, 3, 4,
            1, 3, 5];

        this.indicesLines = [
            2,0,
            2,1,
            2,4,
            2,5,
            3,0,
            3,1,
            3,4,
            3,5,

            5,0,
            5,1,
            4,0,
            4,1,
        ];
    }

    return {
        createVertexData: createVertexData
    }

}());