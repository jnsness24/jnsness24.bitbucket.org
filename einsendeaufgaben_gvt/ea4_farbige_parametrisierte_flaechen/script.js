var gl,
    shaderProgram,
    vertices,
    indicesLines,
    indicesTris,
    matrix = mat4.create(),
    iboLines,
    iboTris,
    formSwitch,
    drawOnlyGrid = false;

// Initial Opening of link has no parameter - so formSwitch is empty. Therefore this gives a default
formSwitch = window.location.search.substr(1);
if (formSwitch.length === 0) {
    formSwitch = "a";
}

initGL();
createShaders();
createVertices();
createBuffer();
draw();


function initGL() {
// Get the WebGL context.
    var canvas = document.getElementById('canvas');

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        canvas.height = window.innerHeight;
        canvas.width = window.innerWidth;
    }


    gl = canvas.getContext('webgl');
    gl.enable(gl.DEPTH_TEST);
    gl.viewport(0, 0, canvas.width, canvas.height);

// Pipeline setup.
    gl.clearColor(.95, .95, .95, 1);
// Backface culling.
    gl.frontFace(gl.CCW);
    gl.disable(gl.CULL_FACE);
    // gl.cullFace(gl.BACK);
}


function createShaders() {
// Compile vertex shader.
    var vsSource = '' +
        'attribute vec4 pos;' +
        'attribute vec4 col;' +
        'uniform mat4 transformMatrix;' +
        'varying vec4 varyingColors;' +
        'void main(){' + 'varyingColors = col;' +
        'gl_Position = transformMatrix * pos;' +
        '}';
    var vs = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vs, vsSource);
    gl.compileShader(vs);

// Compile fragment shader.
    fsSouce = 'precision mediump float;' +
        'varying vec4 varyingColors;' +
        'void main() {' +
        'gl_FragColor = varyingColors;' +
        '}';
    var fs = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fs, fsSouce);
    gl.compileShader(fs);

// Link shader together into a program.
    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vs);
    gl.attachShader(shaderProgram, fs);
    gl.bindAttribLocation(shaderProgram, 0, "pos");
    gl.linkProgram(shaderProgram);
    gl.useProgram(shaderProgram);
}

function createVertices() {
    var n = 66;
    var m = 33;
    // Positions.
    vertices = [];
    indicesLines = [];
    indicesTris = [];
    colors = [];




    // Counter for entries in index array.
    var iLines = 0;
    var iTris = 0;

    // Loop angle t.
    for (var i = 0, t = 0; i <= n; i++, t+=0.1) {
        // Loop radius r.
        for (var j = 0, r = 0; j <= m; j++, r+=0.1) {

            var iVertex = i * (m + 1) + j;
            var x, y, z;


            switch (formSwitch) {
                case "a":
                    x = r * Math.cos(t);
                    y = r * Math.sin(t);
                    z = Math.log(r);
                    break;
                case "b":
                    x = Math.cos(r)*Math.sin(r)*Math.cos(t);
                    y = Math.cos(r)*Math.sin(r)*Math.sin(t);
                    z = Math.cos(r);
                    break;
                case "c":
                    x = Math.sin(r)*2*Math.cos(t);
                    y = Math.cos(r);
                    z = Math.sin(r)*Math.sin(t);
                    break;
                case "d":
                    x = Math.cos(t);
                    y = Math.sin(t*2);
                    z = Math.log(r)*0.5;
                    break;
            }


            // Set vertex positions.
            vertices[iVertex * 3] = x*-0.5;
            vertices[iVertex * 3 + 1] = y*-0.5;
            vertices[iVertex * 3 + 2] = z*-0.5;

            colors.push(r*0.4, 0.3, Math.random(), 1);

            // Set index.
            // Line on beam.
            if (j > 0 && i > 0) {
                indicesLines[iLines++] = iVertex - 1;
                indicesLines[iLines++] = iVertex;
            }
            // Line on ring.
            if (j > 0 && i > 0) {
                indicesLines[iLines++] = iVertex - (m + 1);
                indicesLines[iLines++] = iVertex;
            }

            // Set index.
            // Two Triangles.
            if (j > 0 && i > 0) {
                indicesTris[iTris++] = iVertex;
                indicesTris[iTris++] = iVertex - 1;
                indicesTris[iTris++] = iVertex - (m + 1);
                //
                indicesTris[iTris++] = iVertex - 1;
                indicesTris[iTris++] = iVertex - (m + 1) - 1;
                indicesTris[iTris++] = iVertex - (m + 1);
            }
        }
    }

}


function createBuffer() {
// Setup position vertex buffer object.
    var verticesBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, verticesBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,
        new Float32Array(vertices), gl.STATIC_DRAW);

// Bind vertex buffer to attribute variable.
    var coords = gl.getAttribLocation(shaderProgram, 'pos');
    gl.vertexAttribPointer(coords, 3,
        gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(coords);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //Color data
    var colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,
        new Float32Array(colors), gl.STATIC_DRAW);
    //bind Color buffer to attribute (varying) variable

    var colorsLocation = gl.getAttribLocation(shaderProgram, 'col');
    gl.vertexAttribPointer(colorsLocation, 4,
        gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(colorsLocation);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);


    // Setup lines index buffer object.
    iboLines = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indicesLines), gl.STATIC_DRAW);
    iboLines.numberOfElements = indicesLines.length;
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    // Setup tris index buffer object.
    iboTris = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indicesTris), gl.STATIC_DRAW);
    iboTris.numberOfElements = indicesTris.length;
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);



}


function draw() {
    mat4.rotateX(matrix, matrix, 0.005);
    // mat4.rotateY(matrix, matrix, 0.005);
    mat4.rotateZ(matrix, matrix, 0.005);
    var transformMatrix = gl.getUniformLocation(shaderProgram, "transformMatrix");
    gl.uniformMatrix4fv(transformMatrix, false, matrix);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


    //switchable Overlay of Triangles - if turned off (not be executed) you will only see the grid
    if (drawOnlyGrid === true) {
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
        gl.drawElements(gl.LINES,
            indicesLines.length, gl.UNSIGNED_SHORT, 0);
    }
    else  {
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
        gl.drawElements(gl.TRIANGLES,
            indicesTris.length, gl.UNSIGNED_SHORT, 0);
    }

    requestAnimationFrame(draw);

}

function switchForm(form) {
    var basicUrl = location.protocol + '//' + location.host + location.pathname;
    window.location.href = basicUrl+"?"+form;

}

function switchGridForm() {
    if (drawOnlyGrid == false) {
        drawOnlyGrid = true;
    }
    else {
        drawOnlyGrid = false;
    }
}

