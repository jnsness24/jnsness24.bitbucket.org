/**
 * Created by Jonas on 19.05.2017.
 */
var plane = (function () {
    function createVertexData() {
        var n = 100;
        var m = 100;

        //name added for if-else in drawing method
        this.name = "plane";
        var name = this.name;

        this.colors = [];
        var colors = this.colors;

        // Positions.
        this.vertices =[];
        var vertices = this.vertices;
        // Normals.
        this.normals = [];
        var normals = this.normals;
        // Index data.
        this.indicesLines = [];
        var indicesLines = this.indicesLines;
        this.indicesTris = [];
        var indicesTris = this.indicesTris;

        var du = 20 / n;
        var dv = 20 / m;
        var r = 0.3;
        var R = 0.5;
        // Counter for entries in index array.
        var iLines = 0;
        var iTris = 0;

        for (var i = 0, u = -10; i <= n; i++, u += du) {
            // Loop v.
            for (var j = 0, v = -10; j <= m; j++, v += dv) {

                var iVertex = i * (m + 1) + j;

                var x = u;
                var y = 0;
                var z = v;

                // Set vertex positions.
                vertices[iVertex * 3] = x;
                vertices[iVertex * 3 + 1] = y;
                vertices[iVertex * 3 + 2] = z;

                colors.push(0);
                colors.push(0);
                colors.push(0);
                colors.push(1);


                // Calc and set normals.
                normals[iVertex * 3] = 0;
                normals[iVertex * 3 + 1] = 1;
                normals[iVertex * 3 + 2] = 0;

                // if(i>14){
                // continue;
                // }

                // Set index.
                // Line on beam.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - 1;
                    indicesLines[iLines++] = iVertex;
                }
                // Line on ring.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - (m + 1);
                    indicesLines[iLines++] = iVertex;
                }

                // Set index.
                // Two Triangles.
                if (j > 0 && i > 0) {
                    indicesTris[iTris++] = iVertex;
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                    //
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1) - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                }
            }
        }

    }

    return {
        createVertexData: createVertexData
    }
})();