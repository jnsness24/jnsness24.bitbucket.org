var app = ( function () {
    // pickedForm = window.location.search.substr(1);

    //initial switch for selecting the form and style, for first page visit (local storage == null)
    var gl,
        pickedForm = "ball",
        pickedStyle = "fill",
        prog,
        rotationTorus,
    rotationBalls = [];

    // DELETE THIS FOR SELECTION!!!
    // localStorage.clear();
    //if local Storage is not Null, pick the style and form from local storage
    // if (localStorage.getItem("pickedForm") !== null) {
    //     pickedForm = localStorage.getItem("pickedForm");
    // }
    if (localStorage.getItem("pickedStyle") !== null) {
        pickedStyle = localStorage.getItem("pickedStyle");
    }


    // Array of model objects.
    var models = [];

    var camera = {
        // Initial position of the camera.
        eye: [0, 1, 2],
        // Point to look at.
        center: [0, 0, 0],
        // Roll and pitch of the camera.
        up: [0, 1, 0],
        // Opening angle given in radian.
        // radian = degree*2*PI/360.
        fovy: 60.0 * Math.PI / 180,
        // Camera near plane dimensions:
        // value for left right top bottom in projection.
        lrtb: 2.0,
        // View matrix.
        vMatrix: mat4.create(),
        // Projection matrix.
        pMatrix: mat4.create(),
        // Projection types: ortho, perspective, frustum.
        projectionType: "perspective",
        // Angle to Z-Axis for camera when orbiting the center
        // given in radian.
        zAngle: 0,
        yAngle: 0,
        xAngle: 0,
        // Distance in XZ-Plane from center when orbiting.
        distance: 2
    };

    function start() {
        init();
        render();
    }

    /**
     * sets the value of the html dropdown as localStorage for form - then reloads page
     * @param form
     */
    var setForm = function (form) {
        localStorage.setItem("pickedForm", form);
        location.reload()
    }

    /**
     * sets the value of the html dropdown as localStorage for style - then reloads page
     * @param style
     */
    var setStyle = function (style) {
        localStorage.setItem("pickedStyle", style);
        location.reload();
    }

    /**
     * fill the localstorage with the slider/range value then reload the page
     * @param iterations
     */
    var setIterations = function (iterations) {
        localStorage.setItem("iterations", iterations);
        location.reload();
    }


    function init() {
        initWebGL();
        initShaderProgram();
        initUniforms()
        initModels();
        initEventHandler();
        initPipline();
    }

    function initWebGL() {
        // Get canvas and WebGL context.
        canvas = document.getElementById('canvas');
        gl = canvas.getContext('experimental-webgl');
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;
    }

    /**
     * Init pipeline parameters that will not change again.
     * If projection or viewport change, their setup must
     * be in render function.
     */
    function initPipline() {
        gl.clearColor(.95, .95, .95, 1);

        // Backface culling.
        gl.frontFace(gl.CCW);
        // gl.enable(gl.CULL_FACE);
        gl.cullFace(gl.BACK);

        // Depth(Z)-Buffer.
        gl.enable(gl.DEPTH_TEST);

        // Polygon offset of rastered Fragments.
        gl.enable(gl.POLYGON_OFFSET_FILL);
        gl.polygonOffset(0.5, 0);

        // Set viewport.
        gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);

        // Init camera.
        // Set projection aspect ratio.
        camera.aspect = gl.viewportWidth / gl.viewportHeight;
    }

    function initShaderProgram() {
        // Init vertex shader.
        var vs = initShader(gl.VERTEX_SHADER, "vertexshader");
        // Init fragment shader.
        var fs = initShader(gl.FRAGMENT_SHADER, "fragmentshader");
        // Link shader into a shader program.
        prog = gl.createProgram();
        gl.attachShader(prog, vs);
        gl.attachShader(prog, fs);
        gl.bindAttribLocation(prog, 0, "aPosition");
        gl.linkProgram(prog);
        gl.useProgram(prog);
    }

    /**
     * Create and init shader from source.
     *
     * @parameter shaderType: openGL shader type.
     * @parameter SourceTagId: Id of HTML Tag with shader source.
     * @returns shader object.#
     *
     */
    function initShader(shaderType, SourceTagId) {
        var shader = gl.createShader(shaderType);
        var shaderSource = document.getElementById(SourceTagId).text;
        gl.shaderSource(shader, shaderSource);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.log(SourceTagId + ": " + gl.getShaderInfoLog(shader));
            return null;
        }
        return shader;
    }

    function initUniforms() {
        // Projection Matrix.
        prog.pMatrixUniform = gl.getUniformLocation(prog, "uPMatrix");

        // Model-View-Matrix.
        prog.mvMatrixUniform = gl.getUniformLocation(prog, "uMVMatrix");

        // Color Information for Z Normals
        prog.colorUniform = gl.getUniformLocation(prog, "uColor");

        // Uniform Matrix - because Uniforms for shadows do not rotate with model
        prog.nMatrixUniform = gl.getUniformLocation(prog, "uNMatrix");
    }


    /**
     * only create the model that is the value of pickedForm from local storage -
     * + the plane is always generated
     */
    function initModels() {

        createModel("plane", "wireframe", [0.7, 0.7, 0.7, 1], [1, -1, 1], [0, 0, 0], [1, 1, 1]);

        // switch (pickedForm) {
        //     case "octahedron": {
        //
        //         createModel("octahedron", pickedStyle, [0, 0, 0], [0, 0, 0], [1, 1, 1]);
        //         break;
        //     }
        //     case "ball": {
                createModel("torus", pickedStyle, [1, 0, 1, 1], [0, -1, 0], [0, 0, 0], [1.1, 1.1, 0.8]);
                createModel("ball", pickedStyle, [0, 1, 0, 1], [0, -1, -1], [0, 0, 0], [0.5, 0.5, 1.2]);
                createModel("torus", pickedStyle, [1, 0, 1, 1], [0, -1, -2], [0, 0, 0], [1.1, 1.1, 0.8]);
                createModel("ball", pickedStyle, [0, 1, 0, 1], [0, -1, -4], [0, 0, 0], [0.5, 0.5, 5]);
                // break;
            // }
            // case "torus": {
            //
            //     createModel("torus", pickedStyle, [0, 0, 0], [0, 0, 0], [1, 1, 1]);
            //     break;
            // }
        // }

        rotationTorus = models[1];
        rotationTorus.rotationCounter = 0;

        rotationBalls.push(models[2]);
        rotationBalls[0].radian = 0;

        // rotationBalls.push(models[3]);
        // rotationBalls[1].radian = 0;
        //
        // rotationBalls.push(models[4]);
        // rotationBalls[2].radian = 0;
        //
        // rotationBalls.push(models[5]);
        // rotationBalls[3].radian = 0;
    }


    /**
     * Create model object, fill it and push it in models array.
     *
     * @param geometryname
     * @param fillstyle
     * @param translate
     * @param rotate
     * @param scale
     */
    function createModel(geometryname, fillstyle, color, translate, rotate, scale) {
        var model = {};
        model.fillstyle = fillstyle;
        model.color = color;
        initDataAndBuffers(model, geometryname);
        initTransformations(model, translate, rotate, scale);

        // Create and initialize Model-View-Matrix.
        //deprecated!
        // model.mvMatrix = mat4.create();

        models.push(model);
    }


    function initTransformations(model, translate, rotate, scale) {
        // Store transformation vectors.
        model.translate = translate;
        model.rotate = rotate;
        model.scale = scale;

        model.nMatrix = mat3.create();

        // Create and initialize Model-Matrix.
        model.mMatrix = mat4.create();

        // Create and initialize Model-View-Matrix.
        model.mvMatrix = mat4.create();
    }

    /**
     * Init data and buffers for model object.
     *
     * @parameter model: a model object to augment with data.
     * @parameter geometryname: string with name of geometry.
     */
    function initDataAndBuffers(model, geometryname) {
        // Provide model object with vertex data arrays.
        // Fill data arrays for Vertex-Positions, Normals, Index data:
        // vertices, normals, indicesLines, indicesTris;
        // Pointer this refers to the window.

        // Data from model is always "just" an array and will be formed to Float32Array IN buffercall
        this[geometryname]['createVertexData'].apply(model);


        //Octahedron returns flattenPoints, therefore the buffer load is quite different to other models
        if (model.name == "octahedron") {

            console.log("Model Octahedron will be buffered....");

            model.vboPos = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, model.vboPos);
            gl.bufferData(gl.ARRAY_BUFFER, model.flattenPoints, gl.STATIC_DRAW);


            // Bind vertex buffer to attribute variable.
            model.positionAttrib = gl.getAttribLocation(prog, 'aPosition');
            gl.vertexAttribPointer(model.positionAttrib, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(model.positionAttrib);


        }

        else {
            // Setup position vertex buffer object.
            model.vboPos = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, model.vboPos);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.vertices), gl.STATIC_DRAW);
            // Bind vertex buffer to attribute variable.
            prog.positionAttrib = gl.getAttribLocation(prog, 'aPosition');
            gl.enableVertexAttribArray(prog.positionAttrib);

            // Setup normal vertex buffer object IF the model provides normal information
            if (typeof model.normals != "undefined") {

                console.log(model.name + ": Normal information will be buffered...");

                model.vboNormal = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, model.vboNormal);
                gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.normals), gl.STATIC_DRAW);
                // Bind buffer to attribute variable.
                prog.normalAttrib = gl.getAttribLocation(prog, 'aNormal');
                gl.enableVertexAttribArray(prog.normalAttrib);
            }

            // Setup lines index buffer object If model provides Indices for Lines information
            if (typeof model.indicesLines != "undefined") {

                console.log(model.name + ": Indices for Lines will be buffered....");

                model.iboLines = gl.createBuffer();
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.iboLines);
                gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(model.indicesLines),
                    gl.STATIC_DRAW);
                model.iboLines.numberOfElements = model.indicesLines.length;
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
            }

            // Setup triangle index buffer object If model provides Indices for Triangle information
            if (typeof model.indicesTris != "undefined") {

                console.log(model.name + ": Indices for Triangles will be buffered...");

                model.iboTris = gl.createBuffer();
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.iboTris);
                gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(model.indicesTris), gl.STATIC_DRAW);
                model.iboTris.numberOfElements = model.indicesTris.length;
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
            }


        }
        if (typeof model.colors != "undefined") {

            console.log(model.name + ": Color information will be buffered");

            model.colorBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, model.colorBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.colors), gl.STATIC_DRAW);

            // Bind Color location
            model.colorsLocation = gl.getAttribLocation(prog, 'colors');
            gl.vertexAttribPointer(model.colorsLocation, 4, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(model.colorsLocation);
        }


    }

    function initEventHandler() {

        var deltaRotate = Math.PI / 180;


        window.onkeydown = function (evt) {


            // Change projection of scene.
            switch (evt.keyCode) {

                //Camara Position

                case(79):
                    camera.projectionType = "ortho";
                    camera.lrtb = 2;
                    break;
                case(70):
                    camera.projectionType = "frustum";
                    camera.lrtb = 1.2;
                    break;
                case(80):
                    camera.projectionType = "perspective";
                    break;

                //    Camera Rotation
                case(37):
                    camera.zAngle += deltaRotate * -1;
                    break;
                case(38):
                    camera.xAngle += deltaRotate * 1;
                    break;
                case(39):
                    camera.zAngle += deltaRotate;
                    break;
                case(40):
                    camera.xAngle += deltaRotate * -1;
                    break;

                // Distance Manipulation
                case (187):
                    camera.distance = camera.distance * 0.99;
                    break;
                case (189):
                    camera.distance = camera.distance * 1.01;
                    break;

                // move balls through the torus
                case (75):
                    for (var i = 0; i < rotationBalls.length; i++) {
                        rotationBalls[i].translate[0] += Math.sin(rotationBalls[i].radian) * 0.1;
                        rotationBalls[i].translate[2] += Math.cos(rotationBalls[i].radian) * 0.1;
                        rotationBalls[i].radian += (Math.PI / 36);
                    }

                    // Torus Rotation - Manual Calculation, he waits after one Rotation for 35
                    // event-trigger, so that balls can fly through
                    if (rotationTorus.rotationCounter >= 0 && rotationTorus.rotationCounter <= 36) {
                        rotationTorus.rotate[1] += Math.PI / 37;
                        rotationTorus.rotationCounter++;
                    }
                    else {
                        if (rotationTorus.rotationCounter > 36) {
                            rotationTorus.rotationCounter = -35;
                        }
                        else rotationTorus.rotationCounter++;
                    }
                    break;

                // Model Rotation, 88 = x, 89 = y, 90 =z
                // case (88):
                //     rotationBalls.rotate[0] += deltaRotate;
                //     break;
                // case (89):
                //     rotationBalls.rotate[1] += deltaRotate;
                //     break;
                // case (90):
                //     rotationBalls.rotate[2] += deltaRotate;
                //     break;
            }

            // Render the scene again on any key pressed.
            render();
        };
    }

    /**
     * Run the rendering pipeline.
     */
    function render() {
        // Clear framebuffer and depth-/z-buffer.
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        setProjection();

        // mat4.identity(camera.vMatrix);
        // mat4.rotate(camera.vMatrix, camera.vMatrix, Math.PI/4, [1, 0, 0]);

        calculateCameraOrbit();
        mat4.lookAt(camera.vMatrix, camera.eye, camera.center, camera.up);


        // Loop over models.
        for (var i = 0; i < models.length; i++) {

            gl.uniform4fv(prog.colorUniform, models[i].color);

            // Update modelview for model.
            updateTransformations(models[i]);

            // Set uniforms for model.
            gl.uniformMatrix4fv(prog.mvMatrixUniform, false,
                models[i].mvMatrix);

            gl.uniformMatrix3fv(prog.nMatrixUniform, false,
                models[i].nMatrix);

            draw(models[i]);
        }


    }

    function updateTransformations(model) {

        // Use shortcut variables.
        var mMatrix = model.mMatrix;
        var mvMatrix = model.mvMatrix;

        // Reset matrices to identity.
        mat4.identity(mMatrix);
        mat4.identity(mvMatrix);

        // Translate.
        mat4.translate(mMatrix, mMatrix, model.translate);

        // Rotate
        mat4.rotateX(mMatrix, mMatrix, model.rotate[0]);
        mat4.rotateY(mMatrix, mMatrix, model.rotate[1]);
        mat4.rotateZ(mMatrix, mMatrix, model.rotate[2]);

        // Scale
        mat4.scale(mMatrix, mMatrix, model.scale);

        // Combine view and model matrix
        // by matrix multiplication to mvMatrix.
        mat4.multiply(mvMatrix, camera.vMatrix, mMatrix);

        mat3.normalFromMat4(model.nMatrix, mvMatrix);
    }

    function setProjection() {
        // Set projection Matrix.
        switch (camera.projectionType) {
            case("ortho"):
                var v = camera.lrtb;
                mat4.ortho(camera.pMatrix, -v, v, -v, v, -10, 10);
                break;
            case('frustum'):
                var v = camera.lrtb;
                mat4.frustum(camera.pMatrix, -v / 2, v / 2, -v / 2, v / 2, 1, 10);
                break;
            case('perspective'):
                mat4.perspective(camera.pMatrix, camera.fovy, camera.aspect, 1, 10);
                break;
        }
        // Set projection uniform.
        gl.uniformMatrix4fv(prog.pMatrixUniform, false, camera.pMatrix);
    }

    function draw(model) {
        // Setup position VBO.
        gl.bindBuffer(gl.ARRAY_BUFFER, model.vboPos);
        gl.vertexAttribPointer(prog.positionAttrib, 3, gl.FLOAT, false, 0, 0);

        // if model has color-information, than use this and setup color VBO
        if (typeof model.colors != "undefined") {
            gl.bindBuffer(gl.ARRAY_BUFFER, model.colorBuffer);
            gl.vertexAttribPointer(model.colorsLocation, 4, gl.FLOAT, false, 0, 0);
        }

        // Setup normal VBO. IF normal information exists
        if (typeof model.normals != "undefined") {
            gl.bindBuffer(gl.ARRAY_BUFFER, model.vboNormal);
            gl.vertexAttribPointer(prog.normalAttrib, 3, gl.FLOAT, false, 0, 0);
        }


        // octahedron will be drawn - if "fill" of "fillewireframe" is selected, the primitive is set to TRINAGLE_STRIP
        if (model.name == "octahedron") {

            if (model.fillstyle == "wireframe") {
                gl.uniform4fv(prog.colorUniform, [0,0,1,1]);
                gl.drawArrays(gl.LINE_STRIP, 0, model.flattenPoints.length / 3);
            }
            else if (model.fillstyle == "fill") {
                gl.drawArrays(gl.TRIANGLE_STRIP, 0, model.flattenPoints.length / 3);
            }

            else {
                gl.drawArrays(gl.TRIANGLE_STRIP, 0, model.flattenPoints.length / 3);
            }

        }


        // Setup rendering tris.
        var fill = (model.fillstyle.search(/fill/) != -1);
        if (fill && model.name !== "octahedron") {
            gl.enableVertexAttribArray(prog.normalAttrib);

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.iboTris);
            gl.drawElements(gl.TRIANGLES, model.iboTris.numberOfElements, gl.UNSIGNED_SHORT, 0);
        }

        // Setup rendering lines.
        var wireframe = (model.fillstyle.search(/wireframe/) != -1);
        if (wireframe && model.name !== "octahedron") {
            gl.disableVertexAttribArray(prog.normalAttrib);
            gl.vertexAttrib3f(prog.normalAttrib, 0, 0, 0);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.iboLines);
            gl.drawElements(gl.LINES, model.iboLines.numberOfElements,
                gl.UNSIGNED_SHORT, 0);

        }
    }

    function calculateCameraOrbit() {
        // // Calculate x,z position/eye of camera orbiting the center.

        // Calculate x,z position/eye of camera orbiting the center.
        var x = 0, z = 2;
        camera.eye[x] = camera.center[x];
        camera.eye[z] = camera.center[z];
        camera.eye[x] += camera.distance * Math.sin(camera.zAngle);
        camera.eye[z] += camera.distance * Math.cos(camera.zAngle);

        //
        // var x = 0, y = 1, z = 2;
        //
        // camera.eye[x] = camera.center[x];
        //
        // camera.eye[y] = camera.center[y];
        //
        // camera.eye[z] = camera.center[z];
        //
        // camera.eye[x] += camera.distance * Math.sin(camera.zAngle);
        //
        // camera.eye[y] += camera.distance * Math.sin(camera.xAngle);
        //
        // camera.eye[z] += camera.distance * Math.cos(camera.zAngle);
        //
        // camera.eye[x] = camera.eye[x] * Math.cos(camera.xAngle);
        //
        // camera.eye[z] = camera.eye[z] * Math.cos(camera.xAngle);
        //
        // console.log(camera);
    }


    // App interface.
    return {
        start: start,
        setForm: setForm,
        setStyle: setStyle,
        setIterations: setIterations
    };
}());