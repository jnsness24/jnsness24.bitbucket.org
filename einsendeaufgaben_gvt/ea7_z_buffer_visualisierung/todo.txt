Projektaufgabe: Z-Buffer Visualisierung

Voraussetzungen: Lerneinheiten 1-12 bis einschlie�lich SHA

Bearbeitungszeit: 30 Minuten

Aufgabenstellung: Erstellen Sie eine Szene aus mindestens drei Grundk�rpern die sich gegenseitig �berschneiden. Stellen Sie den Tiefenbuffer, das hei�t die Z-Werte der Fragments im Framebuffer, als Graustufen da. Die Fragments, die in Z-Richtung n�her an der Kamera sind, sollen dunkler dargestellt werden.

Erweiterungen: Keine.

Material: Keines

Bewertungskriterien und Punkte:

Die Bewertung erfolgt an Hand der folgenden Kriterien:

Funktionsf�higkeit der Implementierung: 10 Punkte

Auswahl und Gestaltung der Szene: 5 Punkte

Gesamtpunktzahl: 15