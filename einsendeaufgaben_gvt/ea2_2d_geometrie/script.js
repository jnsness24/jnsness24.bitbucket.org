"use strict";
var gl,
    shaderProgram,
    fishShapeVertices,
    fishShapePositionBuffer,
    fishEyeVertices,
    fishEyePositionBuffer;

initGL();
createShaders();
createVertices();
initBuffers();
draw();


function initGL() {
    var canvas = document.getElementById("canvas");
    gl = canvas.getContext('experimental-webgl');
    gl.clearColor(0, 1, 1, 1);
}

function createShaders() {

    var vsSource = 'attribute vec2 pos;' +
        'void main(){gl_Position = vec4(pos * 0.5, 0.8, 1);' +
        'gl_PointSize = 10.0; }';

    var vs = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vs, vsSource);
    gl.compileShader(vs);

    var fsSouce = 'void main() { gl_FragColor = vec4(0,0,0,1); }';
    var fs = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fs, fsSouce);
    gl.compileShader(fs);


    // attachShader
    //  Fügt ein Shader-Objekt zu einem GPU-Programm hinzu.
    // linkProgram
    //  Verbindet (englisch link) die Shader und erzeugt ein ausführbares GPU-Programm (englisch executable).

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vs);
    gl.attachShader(shaderProgram, fs);
    gl.linkProgram(shaderProgram);
    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "pos");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

}

function createVertices() {
    fishShapeVertices = [
        1.8, 0.08,
        1.77, 0.2,
        1.5, 0.45,
        1, 0.7,
        0.5, 0.86,
        0.2, 0.95,
        0, 1.18,
        -0.18, 1.38,
        -0.2, 1,
        -0.18, 0.7,
        -0.5, 0.58,
        -1, 0.38,
        -1.3, 0.46,
        -1.9, 0.86,
        -1.89, 0.6,
        -1.81, 0.3,
        -1.77, 0,
        -1.87, -0.3,
        -1.94, -0.5,
        -1.97, -0.68,
        -1.5, -0.55,
        -1.1, -0.33,
        -0.08, -0.8,
        -0.13, -1.4,
        0.33, -0.9,
        1, -0.8,
        1.5, -0.5,
        1.8, -0.1,
        1.7, -0.1
    ];

    fishEyeVertices = [
        1.42, 0.2,
        1.35, 0.1,
        1.41, 0,
        1.48, 0.1
    ];

}

function initBuffers() {
    //Buffer for Shape - transforms Array in Float32Array
    fishShapePositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, fishShapePositionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(fishShapeVertices), gl.STATIC_DRAW);


    //Buffer for Eye - transforms Array in Float32Array
    fishEyePositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, fishEyePositionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(fishEyeVertices), gl.STATIC_DRAW);
}


function draw() {
    // Clear framebuffer
    gl.clear(gl.COLOR_BUFFER_BIT);

    //draw shape
    gl.bindBuffer(gl.ARRAY_BUFFER, fishShapePositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 2, gl.FLOAT, false, 0, 0);
    gl.drawArrays(gl.LINE_LOOP, 0, fishShapeVertices.length / 2);

    //draw eye
    gl.bindBuffer(gl.ARRAY_BUFFER, fishEyePositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 2, gl.FLOAT, false, 0, 0);
    gl.drawArrays(gl.LINE_LOOP, 0, fishEyeVertices.length / 2);
}












