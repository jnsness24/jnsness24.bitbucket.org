var ball = ( function () {

    function createVertexData() {
        var n = 32;
        var m = 32;

        //name added for if-else in drawing method
        this.name = "ball";
        var name = this.name;

        // Positions.
        this.vertices = [];
        var vertices = this.vertices;
        // Normals.
        this.normals = [];
        var normals = this.normals;
        // Index data.
        this.indicesLines = [];
        var indicesLines = this.indicesLines;
        this.indicesTris = [];
        var indicesTris = this.indicesTris;

        var du = 2 * Math.PI / n;
        var dv = Math.PI / m;
        var r = 1;

        // Counter for entries in index array.
        var iLines = 0;
        var iTris = 0;

        // Loop angle u.
        for (var i = 0, u = 0; i <= n; i++, u += du) {
            // Loop angle v.
            for (var j = 0, v = 0; j <= m; j++, v += dv) {

                var iVertex = i * (m + 1) + j;

                var x = r * Math.sin(v) * Math.cos(u);
                var y = r * Math.sin(v) * Math.sin(u);
                var z = r * Math.cos(v);

                // Set vertex positions.
                vertices[iVertex * 3] = x;
                vertices[iVertex * 3 + 1] = y;
                vertices[iVertex * 3 + 2] = z;

                // Calc and set normals.
                var vertextLength = Math.sqrt(x*x + y*y + z*z);

                normals[iVertex * 3] = x/vertextLength;
                normals[iVertex * 3 + 1] = y/vertextLength;
                normals[iVertex * 3 + 2] = z/vertextLength;

                // if(i>14){
                // continue;
                // }

                // Set index.
                // Line on beam.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - 1;
                    indicesLines[iLines++] = iVertex;
                }
                // Line on ring.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - (m + 1);
                    indicesLines[iLines++] = iVertex;
                }

                // Set index.
                // Two Triangles.
                if (j > 0 && i > 0) {
                    indicesTris[iTris++] = iVertex;
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                    //
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1) - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                }
            }
        }
    }

    return {
        createVertexData: createVertexData
    }

}());
