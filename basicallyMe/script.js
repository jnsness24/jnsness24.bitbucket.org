/**
 * Created by Jonas on 02.08.2017.
 */


var textArray = [
    "Wunderschön!",
    "bist die Schönste!",
    "Schönes Ding!",
    "Du Süße!!!",
    "geile Alte",
    "knackiger Hintern!",
    "Hübsche du!",
    "Wow!!!",
    "so geil!",
    "was du für schöne Augen hast!!!",
    "Im ernst, deine Augen... wow",
    "was du für schöne Haare hast - hammer",
    "krasse Figur, du trainierst regelmäßig oder?",
    "dein Humor ist einzigartig!"
];

function changePic() {
    var pic = document.querySelector("iframe");
    var randomNumber = Math.floor(Math.random()*400)+1;
    var url = "https://instanerd.me/w/" + randomNumber;
    console.log(randomNumber);
    console.log(pic);
    pic.setAttribute("src",url);
}

function generateCompliments() {
    var divsize = ((Math.random() * 100) + 50).toFixed();
    var color = '#' + Math.round(0xffffff * Math.random()).toString(16);
    $newdiv = $('<div/>').css({
        'width': 700 + 'px',
        'height': 300 + 'px',
    });

    var randomWord = textArray[(Math.floor(Math.random() * textArray.length))]
    var posx = (Math.random() * ($(document).width() - divsize)).toFixed();
    var posy = (Math.random() * ($(document).height() - divsize)).toFixed();
    var randomSize = (Math.random() * (100 - 20) + 20);

    $newdiv.text(randomWord).css({
        'left': posx + 'px',
        'top': posy + 'px',
        'display': 'none',
        'color': color,
        'font-size': randomSize + 'px'
    }).appendTo('#third').fadeIn(200).delay(500).fadeOut(500, function() {
        $(this).remove();
        generateCompliments();
    });
}